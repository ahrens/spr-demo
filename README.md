**Networkit Installieren und Ausprobieren**

Einrichten einer *virtual environment* für Python: nutzereigene Installation inclusive der Möglichkeit eignene Module (networkit u.a.) zu installieren

`cd`

`python3 -m venv pyenv`

Legt lokal im Homeverzeichnis ein Verzeichnis `pyenv` mit komplett eigenständiger Python-Umgebung an

Aktivierbar mittels

`source pyenv/bin/activate.csh`

Deaktivierbar mittels `deactivate`

Neuestes `pip3` 

`pip3 install --upgrade pip`

Cython installieren

`pip3 install cython`

(irgendwo) ein Unterverzeichnis (u.a.) für NetworKit einrichten, dort

`git clone https://github.com/networkit/networkit.git`

`
cd networkit
git submodule update --init
`

und 

`pip3 install -e .`

`ipython3` Interaktive Python-Shell

`In [1]: import networkit as nk` 

`In [2]: G=nk.readGraph("input/jazz.graph", nk.Format.METIS)` 

`In [3]: e=nk.embedding.Node2Vec(G) `

`In [4]: e.run() `

`In [5]: print(e.getFeatures()) `











